module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },
        'http-server': {
            'dev': {
                // the server root directory
                root: "",
                port: 9000,
                host: "127.0.0.1",
                cache: 0,
                showDir: true,
                autoIndex: true,
                runInBackground: false
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-http-server');

    grunt.registerTask('default', ['jshint']);

};