'use strict';

angular.module('hackathonApp', ['ngRoute', 'ngAnimate', 'ngResource', 'ngCookies'])
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'views/about.html',
                    controller: 'AboutCtrl'
                })
                .when('/subscribe', {
                    templateUrl: 'views/subscribe.html',
                    controller: 'SubscribeCtrl'
                })
                .when('/terms', {
                    templateUrl: 'views/terms.html',
                    controller: 'TermsCtrl'
                })
                .when('/faq', {
                    templateUrl: 'views/faq.html',
                    controller: 'FAQCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }])
    .controller('AboutCtrl', ['$route', '$routeParams', '$location', '$scope', '$anchorScroll',
        function($route, $routeParams, $location, $scope, $anchorScroll) {
            $scope.scrollTo = function(id) {
                $location.hash(id);
                $anchorScroll();
            }

            $scope.teams = [
                {
                    teamname: 'Senses Forms',
                    challenge: 'internalize the way (Quinity) forms are displayed inside the Senses app',
                    email: 'walter.brand@rabobank.nl'
                },
                {
                    teamname: 'Rabo Light',
                    challenge: 'create a eco system of light versions of Rabobank apps',
                    email: 'Reinier.de.Kroes@rabobank.nl'
                },
                {
                    teamname: 'HARIKAN',
                    challenge: 'improve the ease of use of the personified Work Item Forward',
                    email: 'harikrishnan.pandian@rabobank.nl'
                },
                {
                    teamname: 'Integrators',
                    challenge: 'reduce manual effort in clearing the blocked queues because of purchase order interface failures',
                    email: 'Karthikeyan.null02@rabobank.nl'
                },
                {
                    teamname: 'Path Finders',
                    challenge: 'automate the Open / Close CO Period and optimize the performance of the IDB error report',
                    email: 'balakrishnan.ganesan@rabobank.nl'
                },
                {
                    teamname: 'MicroBoys',
                    challenge: 'prove that the Rabobank Online environment needs to migrate to a microservices architecture. We need that because: 1) We need to build, deploy and scale applications independently more and more. 2) We want teams to be autonomous (DevOps). Yet, we still want to be able to monitor/tune/control (operate) the whole',
                    email: 'harry.metske@rabobank.nl'
                },
                {
                    teamname: 'Sparkles',
                    challenge: 'optimize the error report',
                    email: 'Anusha.Anusha@rabobank.nl'
                },
                {
                    teamname: 'Rovers',
                    challenge: 'create a custom filter to enhance the search functionality of the UI Addon',
                    email: 'Manikandan.Manikandan@rabobank.nl'
                },
                {
                    teamname: 'Memory Saver',
                    challenge: 'design a studio application for displaying the total memory of the HANA database and cleaning up the junk files on a regular database',
                    email: 'Ravi-kiran.Ramachandrula@rabobank.nl'
                },
                {
                    teamname: 'Report Validator',
                    challenge: 'be able to compare BW reports',
                    email: 's.t.sudhakar@rn.rabobank.nl'
                },
                {
                    teamname: 'Troy',
                    challenge: 'improve handling of the template product from UI addon',
                    email: 's.t.sudhakar@rn.rabobank.nl'
                },
                {
                    teamname: 'Mavericks',
                    challenge: 'to display default values for all dimensions in BPC input sheets and reports',
                    email: 'Bindu-Priya.Chaluvadi@rabobank.nl'
                },
                {
                    teamname: 'Panchathanthra',
                    challenge: 'create a dashboard with the following functionalities in HANA using Design Studio a. where used list of HANA tables b. Table dataload statistics for HANA tables. 2. ABAP Report to delete the Test objects that is created in a system 3. Customized component in Design Studio - Quadrant Chart using SDK ',
                    email: 'Pradheepa.Balakrishnan@rabobank.nl'
                },
                {
                    teamname: 'Droid team',
                    challenge: 'build a voice based money transfer to buddy accounts which also helps visually challenged customers',
                    email: 'Hari-Hara-Kumar.Dhanakoti@rabobank.nl'
                },
                {
                    teamname: 'IOT-Honks',
                    challenge: 'implement location based customer engagement using beacon low emitter Bluetooth devices',
                    email: 'Saravanan.Shanmugavel@rabobank.nl'
                }
            ];

            angular.element("#menu").show();

        }])
    .controller('SubscribeCtrl', ['$routeParams', '$scope', '$http', function($routeParams, $scope, $http) {
        $scope.newTeam = {};

        angular.element("#menu").hide();
        $scope.saveTeam = function() {
            $http({
                url: 'http://hackathon.portaal.rabobank.nl/restServer/example/subscribe',
                method: 'POST',
                data: 'teamname=' + encodeURIComponent($scope.newTeam.teamname) +
                    '&teamcaptain=' + encodeURIComponent($scope.newTeam.teamcaptain) +
                    '&teamcaptaintel=' + encodeURIComponent($scope.newTeam.teamcaptaintel) +
                    '&email=' + encodeURIComponent($scope.newTeam.email) +
                    '&deelnemerNaamEen=' + encodeURIComponent($scope.newTeam.deelnemerNaam.een) +
                    '&deelnemerDieetEen=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.een) +
                    '&deelnemerPasnummerEen=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.een) +
                    '&deelnemerNaamTwee=' + encodeURIComponent($scope.newTeam.deelnemerNaam.twee) +
                    '&deelnemerDieetTwee=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.twee) +
                    '&deelnemerPasnummerTwee=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.twee) +
                    '&deelnemerNaamDrie=' + encodeURIComponent($scope.newTeam.deelnemerNaam.drie) +
                    '&deelnemerDieetDrie=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.drie) +
                    '&deelnemerPasnummerDrie=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.drie) +
                    '&deelnemerNaamVier=' + encodeURIComponent($scope.newTeam.deelnemerNaam.vier) +
                    '&deelnemerDieetVier=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.vier) +
                    '&deelnemerPasnummerVier=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.vier) +
                    '&deelnemerNaamVijf=' + encodeURIComponent($scope.newTeam.deelnemerNaam.vijf) +
                    '&deelnemerDieetVijf=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.vijf) +
                    '&deelnemerPasnummerVijf=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.vijf) +
                    '&deelnemerNaamZes=' + encodeURIComponent($scope.newTeam.deelnemerNaam.zes) +
                    '&deelnemerDieetZes=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.zes) +
                    '&deelnemerPasnummerZes=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.zes) +
                    '&deelnemerNaamZeven=' + encodeURIComponent($scope.newTeam.deelnemerNaam.zeven) +
                    '&deelnemerDieetZeven=' + encodeURIComponent($scope.newTeam.deelnemerDieetwensen.zeven) +
                    '&deelnemerPasnummerZeven=' + encodeURIComponent($scope.newTeam.deelnemerPasnummer.zeven) +
                    '&bhv=' + encodeURIComponent($scope.newTeam.bhv) +
                    '&challenge=' + encodeURIComponent($scope.newTeam.challenge),
                headers: {'Content-type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.failure !== undefined) {
                    $scope.sendError = true;
                    $timeout(function(){
                        $scope.sendError = false;
                    }, 5000);
                    console.log('Error: ', data, status, headers, config)
                } else {
                    $scope.sendSuccessfully = true;
                    $timeout(function(){
                        $scope.sendSuccessfully = false;
                    }, 5000);
                    console.log('Success: ', data, status, headers, config)
                }
            }).
            error(function(data, status, headers, config) {
                $scope.sendError = true;
                $timeout(function(){
                    $scope.sendError = false;
                }, 5000);
                console.log('Error: ', data, status, headers, config)
            })
        };
    }])
    .controller('FAQCtrl', ['$routeParams', '$scope', function() {
        angular.element("#menu").hide();
    }])
    .controller('TermsCtrl', ['$routeParams', '$scope', '$http', '$timeout', '$cookies', function($routeParams, $scope, $http, $timeout, $cookies) {
        angular.element("#menu").hide();

        $scope.terms = {};

        $http.jsonp('http://raboweblogin.rabobank.nl/rwalogin/isapilogin.dll').success(function (data) {
            setNameOfUser();
            console.log('Success: ' + data);
        }).error(function (data) {
            setNameOfUser();
            console.log('Error: ' + data);
        });

        function setNameOfUser() {
            var raboSecCookie = $cookies.get("rabosec.ui");
            if(raboSecCookie !== undefined) {
                var raboSecCookieUID = raboSecCookie.split("&")[0];
                $scope.userId = raboSecCookieUID.substr(raboSecCookieUID.indexOf("=") + 1);

                $http({
                    url: 'http://hackathon.portaal.rabobank.nl/restServer/adget.php?userid=' + $scope.userId,
                    method: 'GET'
                }).success(function (data) {
                    $scope.nameUser = data.displayname;
                    $scope.terms.email = data.mail;

                    $scope.disableTermsEmail = true;
                }).error(function (data, status, headers, config) {
                    console.log('Error: ', data, status, headers, config)
                })
            }
        }

        $scope.saveConfirmationTerms = function() {
            $http({
                url: 'http://hackathon.portaal.rabobank.nl/restServer/example/terms',
                method: 'POST',
                data: 'nameUser=' + encodeURIComponent($scope.nameUser) +
                      '&emailUser=' + encodeURIComponent($scope.terms.email) +
                      '&userId=' + encodeURIComponent($scope.userId),
                headers: {'Content-type': 'application/x-www-form-urlencoded'}
            }).
                success(function(data, status, headers, config) {
                    if(data.failure !== undefined) {
                        $scope.sendError = true;
                        $timeout(function () {
                            $scope.sendError = false;
                        }, 5000);
                        console.log('Error: ', data, status, headers, config)
                    } else if(data.alreadyAgreed !== undefined) {
                        $scope.alreadyAgreed = true;
                        $timeout(function(){
                            $scope.alreadyAgreed = false;
                        }, 5000);
                        console.log('Error: ', data, status, headers, config)
                    } else {
                        $scope.sendSuccessfully = true;
                        $timeout(function(){
                            $scope.sendSuccessfully = false;
                        }, 5000);
                        console.log('Success: ', data, status, headers, config)
                    }
                }).
                error(function(data, status, headers, config) {
                  $scope.sendError = true;
                    $timeout(function(){
                        $scope.sendError = false;
                    }, 5000);
                    console.log('Error: ', data, status, headers, config)
                })
        };

    }]);