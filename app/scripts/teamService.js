angular.module('hackathonApp').factory('teamService', function($resource) {
    return $resource(
        'http://hackathon.geertjan.it/restServer/example/:id',
        { id: '@id' },
        { update: { method: 'PUT' } }
    );
});
