CREATE DATABASE HACKATHON;

USE HACKATHON;

CREATE USER 'hackathon'@'localhost' IDENTIFIED BY 'rabobank01';

GRANT ALL PRIVILEGES ON HACKATHON.* TO 'hackathon'@'localhost';
GRANT ALL PRIVILEGES ON HACKATHON.* TO 'root'@'localhost';
GRANT ALL PRIVILEGES ON HACKATHON.* TO 'hackathon'@'%';
GRANT ALL PRIVILEGES ON HACKATHON.* TO 'root'@'%';
GRANT ALL PRIVILEGES ON HACKATHON.* TO 'hackathon'@'rb344827.rabobank.corp';
GRANT ALL PRIVILEGES ON HACKATHON.* TO 'root'@'rb344827.rabobank.corp';

-- DROP TABLE IF EXISTS termagrees;
CREATE TABLE termagrees (userid VARCHAR(120) NOT NULL, nameuser VARCHAR(120) NOT NULL, email VARCHAR(240) NOT NULL);

ALTER TABLE termagrees ADD CONSTRAINT TERMAGREES_PK PRIMARY KEY (userid);

-- INSERT INTO termagrees (userid, nameuser, email) values ('3002629718', 'Boer de, GJ (Geert-Jan)', 'G.J.Boer@rn.rabobank.nl');
CREATE TABLE subscriptions (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY
                            , teamname VARCHAR(180) NOT NULL
                            , teamcaptain VARCHAR(180) NOT NULL
                            , teamcaptaintel VARCHAR(180) NOT NULL
                            , email VARCHAR(240) NOT NULL
                            , deelnemerNaamEen VARCHAR(180) NOT NULL
                            , deelnemerDieetEen VARCHAR(180) NOT NULL
                            , deelnemerPasnummerEen VARCHAR(180) NOT NULL
                            , deelnemerNaamTwee VARCHAR(180) NOT NULL
                            , deelnemerDieetTwee VARCHAR(180) NOT NULL
                            , deelnemerPasnummerTwee VARCHAR(180) NOT NULL
                            , deelnemerNaamDrie VARCHAR(180) NOT NULL
                            , deelnemerDieetDrie VARCHAR(180) NOT NULL
                            , deelnemerPasnummerDrie VARCHAR(180) NOT NULL
                            , deelnemerNaamVier VARCHAR(180) NOT NULL
                            , deelnemerDieetVier VARCHAR(180) NOT NULL
                            , deelnemerPasnummerVier VARCHAR(180) NOT NULL
                            , deelnemerNaamVijf VARCHAR(180) NOT NULL
                            , deelnemerDieetVijf VARCHAR(180) NOT NULL
                            , deelnemerPasnummerVijf VARCHAR(180) NOT NULL
                            , deelnemerNaamZes VARCHAR(180) NOT NULL
                            , deelnemerDieetZes VARCHAR(180) NOT NULL
                            , deelnemerPasnummerZes VARCHAR(180) NOT NULL
                            , deelnemerNaamZeven VARCHAR(180) NOT NULL
                            , deelnemerDieetZeven VARCHAR(180) NOT NULL
                            , deelnemerPasnummerZeven VARCHAR(180) NOT NULL
                            , bhv VARCHAR(180) NOT NULL
                            , challenge VARCHAR(180) NOT NULL);

commit;
