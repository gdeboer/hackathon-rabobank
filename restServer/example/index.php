<?php

require '../RestServer.php';
require 'SubscribeController.php';
require 'TermsController.php';
require 'connection.inc.php';

$server = new RestServer('debug');
$server->addClass('SubscribeController');
$server->addClass('TermsController');
$server->handle();
