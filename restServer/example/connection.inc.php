<?php
/**
 * Standaard include voor functies
 *
 * @author P.P. van Mourik <pvmourik@tyrion.nl>
 * @version 1.0.2
 * @copyright Peter van Mourik
 * @since 01-09-2003
 * @change 2004-05-16 Commentaar toegevoegd
 * @change 2004-05-16 Opgeschoond
 * @change 2004-08-10 Alleen nog de class
 * @change 2005-08-02 Debug functionaliteit en tellen # queries toegevoegd
 * @change 2015-04-09 mysql vervangen door mysqli
 * @change 2015-04-09 prepared statements toegevoegd
 */

/**
 * Database connectivity class
 *
 * Door het aanroepen van deze class wordt een MySQL connectie aangeroepen.
 * Gebruik de volgende code om deze te gebruiken:
 * <code>
 * new Connection
 * </code>
 * @package PHProjekt
 */
class Connection {
    /**
     * Naam van de server
     * @access public
     * @var string
     */
    var $hostname = NULL;
    /**
     * Naam van de Database
     * @access public
     * @var string
     */
    var $database = NULL;
    /**
     * De username van de connectie
     * @access public
     * @var string
     */
    var $username = NULL;
    /**
     * Het password voor de connectie
     * @access public
     * @var string Het password
     */
    var $password = NULL;
    /**
     * Het ID van de Link
     * @access public
     * @var string
     */
    var $link_id = NULL;
    /**
     * Het ID van de Query
     * @access public
     * @var string
     */
    var $query_id = NULL;
    /**
     * Het Statement van een prepared query
     * @access public
     * @var string
     */
    var $stmt = NULL;
    /**
     * Number of queries on this connection
     * @access public
     * @var integer
     */
    var $numqueries = NULL;

    /**
     * Handle voor filename
     * @access private
     * @var handle
     */
    var $handle = NULL;

    /**
     * Handle voor filename
     * @access private
     * @var handle
     */
    var $debug = 0;

    /**
     * String voor de filename
     * @access public
     * @var string
     */
    var $filename = 'query.log';
    /**
     * String voor de rows
     * @access public
     * @var string
     */
    var $row = NULL;
    /**
     * Functie om een database connectie te maken
     *
     * Omdat deze functie dezelfde naam als de class heeft,
     * wordt deze functie automatisch aangeroepen zodra een
     * nieuwe instantie van de class wordt gemaakt.
     * {@source }
     */
    function Connection ($host = "145.72.30.160",
                         $db   = "testsupply",
                         $user = "root",
                         $pass = "rabobank01") {
        $this->hostname = $host;
        $this->username = $user;
        $this->password = $pass;
        $this->database = $db;
        $this->link_id  = mysqli_connect($this->hostname,
            $this->username,
            $this->password) or
        die(mysqli_error($this->link_id));
        $this->Usedb($this->database);
    }

    /**
     * Functie voor het selecteren van de database
     * {@source }
     */
    function Usedb ($db="test") {
        mysqli_select_db($this->link_id, $db) or die("Database connectie niet mogelijk: " . mysqli_error($this->link_id));
    }

    /**
     * Functie om laatste ID op te halen
     * {@source }
     */
    function GetID () {
        return mysqli_insert_id($this->link_id);
    }

    /**
     * Functie voor het uitvoeren van een query
     * {@source }
     */
    function Query ($query) {
        if ($this->debug==1) {
            $this->handle = fopen($this->filename,"a+");
            fwrite($this->handle, $query."\n");
            fclose($this->handle);
        }
        $this->query_id = mysqli_query($this->link_id,$query) or die("Unable to execute query: " . mysqli_error($this->link_id));
        $this->numqueries++;
    }

    /**
     * Functie voor het uitvoeren van een query
     * {@source }
     */
    function QueryPrepare ($query, $param_type, $a_bind_params) {
        /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
        $a_params = array();

        $n = count($a_bind_params);

        /* with call_user_func_array, array params must be passed by reference */
        $a_params[] = & $param_type;

        for($i = 0; $i < $n; $i++) {
            /* with call_user_func_array, array params must be passed by reference */
            $a_params[] = & $a_bind_params[$i];
        }

        $this->stmt = mysqli_prepare($this->link_id,$query) or die("Unable to execute query: " . mysqli_error($this->link_id));

        call_user_func_array(array($this->stmt, 'bind_param'), $a_params);
        $this->stmt->execute();

        $meta = $this->stmt->result_metadata();
        if ($meta) {
			while ($field = $meta->fetch_field())
			{
				$params[] = &$this->row[$field->name];
			}

			call_user_func_array(array($this->stmt, 'bind_result'), $params);
		}

        $this->numqueries++;
    }

    /**
     * Functie voor het uitvoeren van een query met teruggave van ID
     * {@source }
     */
    function QueryReturn ($query) {
        $this->query_id = mysqli_query($this->link_id,$query) or die("Unable to execute query: " . mysqli_error($this->link_id));
        return mysqli_insert_id($this->link_id);
    }

    /**
     * Functie voor het ophalen van een rij uit een query
     * {@source }
     */
    function Fetch () {
        if ($this->query_id) {
            return mysqli_fetch_assoc($this->query_id);
        } else {
            return FALSE;
        }
    }

    /**
     * Functie voor het ophalen van een rij uit een query
     * {@source }
     */
    function FetchPrepared () {
        if ($this->stmt->fetch()==null) return false;
        return $this->row;
    }

    /**
     * Functie voor het vrijmaken van de connectie na een prepared statement
     * {@source }
     */
    function Free () {
        return $this->stmt->close();
    }

    /**
     * Functie voor het sluiten van een database connectie
     * {@source }
     */
    function Close () {
        mysqli_close($this->link_id);
    }

    /**
     * Functie voor het ophalen van het aantal rijen in een query
     * {@source }
     */
    function Numrows () {
        if ($this->query_id) {
            return mysqli_num_rows($this->query_id);
        } else {
            return FALSE;
        }
    }

}
?>