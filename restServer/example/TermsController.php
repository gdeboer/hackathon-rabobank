<?php

class TermsController
{
    /**
     * agree with the terms of the hackathon
     *
     * @url POST /terms
     */
    public function terms()
    {
        $nameuser = $_POST['nameUser'];
        $mailuser = $_POST['emailUser'];
        $userid = $_POST['userId'];

        // validate
        if (empty($nameuser) or empty($mailuser) or empty($userid)) {
            return array("failure" => "Not all values provided. name user " . $nameuser . " mail " . $mailuser . " userid " . $userid);
        }

        // persist to the database
        $conn = mysql_connect('127.0.0.1', 'hackathon', 'rabobank01');
        $database = mysql_select_db('HACKATHON');
        if (!$conn | !$database) {
            return array("failure" => "Connection to database failed");
        }

        $alreadyExistsQuery = sprintf("select * from termagrees where userid = '%s'",
            mysql_real_escape_string($userid));

        $result = mysql_query($alreadyExistsQuery, $conn);

        // Check result
        // This shows the actual query sent to MySQL, and the error. Useful for debugging.
        if (!$result) {
            $message  = 'Invalid query: ' . mysql_error() . "\n";
            $message .= 'Whole query: ' . $query;
            return array("failure" => $message);
        }

        $num_rows = mysql_num_rows($result);
        if($num_rows > 0) {
            return array("alreadyAgreed" => "Terms have already been accepted");
        }

        $query = sprintf("INSERT INTO termagrees (userid, nameuser, email) VALUES ('%s', '%s', '%s')",
            mysql_real_escape_string($userid),
            mysql_real_escape_string($nameuser),
            mysql_real_escape_string($mailuser));

        // Perform Query
        $result = mysql_query($query, $conn);

        // Check result
        // This shows the actual query sent to MySQL, and the error. Useful for debugging.
        if (!$result) {
            $message  = 'Invalid query: ' . mysql_error() . "\n";
            $message .= 'Whole query: ' . $query;
            return array("failure" => $message);
        }

        mysql_close($conn);

        // mail the user
        $agreed = "You agreed to the terms of the hackathon";

        require_once('PHPMailerAutoload.php');
        $email = new PHPMailer();
        $email->isSMTP();
        $email->Host = 'localhost';
        $email->From      = 'p.p.mourik@rn.rabobank.nl';
        $email->FromName  = 'Hackathon Website';
        $email->Subject   = 'Hackathon Agreed with terms';
        $email->Body      = "By : ".$nameuser." <br/>";
        $email->AddAddress( 'p.p.mourik@rn.rabobank.nl' );
        $email->AddAddress( 'g.j.boer@rn.rabobank.nl' );
        $email->AddAddress($mailuser);
        $email->isHTML(true);
        $email->Send();

        return array("success" => $agreed);
    }
}
