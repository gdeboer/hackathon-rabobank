<?php

class SubscribeController
{
    /**
     * subscribe for the hackathon
     *
     * @url POST /subscribe
     */
    public function subscribe()
    {
        $teamname = $_POST['teamname'];
        $teamcaptain = $_POST['teamcaptain'];
        $teamcaptaintel  = $_POST['teamcaptaintel'];
        $email = $_POST['email'];
        $deelnemerNaamEen = $_POST['deelnemerNaamEen'];
        $deelnemerDieetEen = $_POST['deelnemerDieetEen'];
        $deelnemerPasnummerEen = $_POST['deelnemerPasnummerEen'];
        $deelnemerNaamTwee = $_POST['deelnemerNaamTwee'];
        $deelnemerDieetTwee = $_POST['deelnemerDieetTwee'];
        $deelnemerPasnummerTwee = $_POST['deelnemerPasnummerTwee'];
        $deelnemerNaamDrie = $_POST['deelnemerNaamDrie'];
        $deelnemerDieetDrie = $_POST['deelnemerDieetDrie'];
        $deelnemerPasnummerDrie = $_POST['deelnemerPasnummerDrie'];
        $deelnemerNaamVier = $_POST['deelnemerNaamVier'];
        $deelnemerDieetVier = $_POST['deelnemerDieetVier'];
        $deelnemerPasnummerVier = $_POST['deelnemerPasnummerVier'];
        $deelnemerNaamVijf = $_POST['deelnemerNaamVijf'];
        $deelnemerDieetVijf = $_POST['deelnemerDieetVijf'];
        $deelnemerPasnummerVijf = $_POST['deelnemerPasnummerVijf'];
        $deelnemerNaamZes = $_POST['deelnemerNaamZes'];
        $deelnemerDieetZes = $_POST['deelnemerDieetZes'];
        $deelnemerPasnummerZes = $_POST['deelnemerPasnummerZes'];
        $deelnemerNaamZeven = $_POST['deelnemerNaamZeven'];
        $deelnemerDieetZeven = $_POST['deelnemerDieetZeven'];
        $deelnemerPasnummerZeven = $_POST['deelnemerPasnummerZeven'];
        $bhv = $_POST['bhv'];
        $challenge = $_POST['challenge'];

        // validate
        if (empty($teamname) or empty($teamcaptain) or empty($email)) {
            return array("failure" => "Not all values provided. teamname " . $teamname . " teamcaptain " . $teamcaptain . " mail " . $email);
        }

        // persist to the database
        $conn = mysql_connect('127.0.0.1', 'hackathon', 'rabobank01');
        $database = mysql_select_db('HACKATHON');
        if (!$conn | !$database) {
            return array("failure" => "Connection to database failed");
        }

        $query = sprintf("INSERT INTO subscriptions (teamname, teamcaptain, email, deelnemerNaamEen, deelnemerDieetEen, deelnemerPasnummerEen, deelnemerNaamTwee, deelnemerDieetTwee, deelnemerPasnummerTwee, deelnemerNaamDrie, deelnemerDieetDrie, deelnemerPasnummerDrie, deelnemerNaamVier, deelnemerDieetVier, deelnemerPasnummerVier, deelnemerNaamVijf, deelnemerDieetVijf, deelnemerPasnummerVijf, deelnemerNaamZes, deelnemerDieetZes, deelnemerPasnummerZes, deelnemerNaamZeven, deelnemerDieetZeven, deelnemerPasnummerZeven, bhv, challenge) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
            mysql_real_escape_string($teamname),
            mysql_real_escape_string($teamcaptain),
            mysql_real_escape_string($email),
            mysql_real_escape_string($deelnemerNaamEen),
            mysql_real_escape_string($deelnemerDieetEen),
            mysql_real_escape_string($deelnemerPasnummerEen),
            mysql_real_escape_string($deelnemerNaamTwee),
            mysql_real_escape_string($deelnemerDieetTwee),
            mysql_real_escape_string($deelnemerPasnummerTwee),
            mysql_real_escape_string($deelnemerNaamDrie),
            mysql_real_escape_string($deelnemerDieetDrie),
            mysql_real_escape_string($deelnemerPasnummerDrie),
            mysql_real_escape_string($deelnemerNaamVier),
            mysql_real_escape_string($deelnemerDieetVier),
            mysql_real_escape_string($deelnemerPasnummerVier),
            mysql_real_escape_string($deelnemerNaamVijf),
            mysql_real_escape_string($deelnemerDieetVijf),
            mysql_real_escape_string($deelnemerPasnummerVijf),
            mysql_real_escape_string($deelnemerNaamZes),
            mysql_real_escape_string($deelnemerDieetZes),
            mysql_real_escape_string($deelnemerPasnummerZes),
            mysql_real_escape_string($deelnemerNaamZeven),
            mysql_real_escape_string($deelnemerDieetZeven),
            mysql_real_escape_string($deelnemerPasnummerZeven),
            mysql_real_escape_string($bhv),
            mysql_real_escape_string($challenge));

        // Perform Query
        $result = mysql_query($query, $conn);

        // Check result
        // This shows the actual query sent to MySQL, and the error. Useful for debugging.
        if (!$result) {
            $message  = 'Invalid query: ' . mysql_error() . "\n";
            $message .= 'Whole query: ' . $query;
            return array("failure" => $message);
        }

        mysql_close($conn);

        // mail the user
        $aanmelding = "Subscribed: " . $teamname . " for challenge " . $challenge . " with email " . $email;

        require_once('PHPMailerAutoload.php');
        $email = new PHPMailer();
        $email->isSMTP();
        $email->Host = 'localhost';
        $email->From      = 'p.p.mourik@rn.rabobank.nl';
        $email->FromName  = 'Hackathon Website';
        $email->Subject   = 'Hackathon Aanmelding';
        $email->Body      = "Hackathon aanmelding voor team : ".$teamname." <br/>".
            "Team captain : " . $teamcaptain . " <br/>".
            "Team captain telefoon : " . $teamcaptaintel . " <br/>".
            "Deelnemer 1 : " . $deelnemerNaamEen . " <br/>".
            "Dieetwensen 1 : " . $deelnemerDieetEen . " <br/>".
            "Pasnummer 1 : " . $deelnemerPasnummerEen . " <br/>".
            "Deelnemer 2 : " . $deelnemerNaamTwee . " <br/>".
            "Dieetwensen 2 : " . $deelnemerDieetTwee . " <br/>".
            "Pasnummer 2 : " . $deelnemerPasnummerTwee . " <br/>".
            "Deelnemer 3 : " . $deelnemerNaamDrie . " <br/>".
            "Dieetwensen 3 : " . $deelnemerDieetDrie . " <br/>".
            "Pasnummer 3 : " . $deelnemerPasnummerDrie . " <br/>".
            "Deelnemer 4 : " . $deelnemerNaamVier . " <br/>".
            "Dieetwensen 4 : " . $deelnemerDieetVier . " <br/>".
            "Pasnummer 4 : " . $deelnemerPasnummerVier . " <br/>".
            "Deelnemer 5 : " . $deelnemerNaamVijf . " <br/>".
            "Dieetwensen 5 : " . $deelnemerDieetVijf . " <br/>".
            "Pasnummer 5 : " . $deelnemerPasnummerVijf . " <br/>".
            "Deelnemer 6 : " . $deelnemerNaamZes . " <br/>".
            "Dieetwensen 6 : " . $deelnemerDieetZes . " <br/>".
            "Pasnummer 6 : " . $deelnemerPasnummerZes . " <br/>".
            "Deelnemer 7 : " . $deelnemerNaamZeven . " <br/>".
            "Dieetwensen 7 : " . $deelnemerDieetZeven . " <br/>".
            "Pasnummer 7 : " . $deelnemerPasnummerZeven . " <br/>".
            "BHV : " . $bhv . " <br/>".
            "Challenge : " . $_POST['challenge'] . " <br/>".
            "Email : " . $_POST['email'] . " <br/>";
        $email->AddAddress( 'Amerik.van.Rijn@rabobank.nl' );
        $email->AddAddress( 'Geert-Jan.de.Boer@rabobank.nl' );
        $email->isHTML(true);
        $email->Send();

        return array("success" => $aanmelding);
    }
}
