<?php
// File staat ook in LTE map
if (isset($_GET['userid'])) {
    $userid = $_GET['userid'];

    // connect to ldap server
    $ldapconn = ldap_connect("ADDC0007.rabobank.corp");

    if ($ldapconn) {
        // binding anonymously
        $ldapbind = ldap_bind($ldapconn, "svctestsupply@rabobank.corp", "E25[g&Kv");

        if ($ldapbind) {
            ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

            $dn = "OU=End Users,OU=XP Accounts,OU=DTB Desktop,DC=rabobank,DC=corp";
            $filter = "(rabo-ID=" . rawurlencode($userid) . ")";
            // allow * character
            $filter = str_replace("%2A", "*", $filter);
            $justthese = array("displayname", "SAMAccountName", "rabo-ID", "thumbnailPhoto", "mail");

            $sr = ldap_search($ldapconn, $dn, $filter, $justthese);

            $info = ldap_get_entries($ldapconn, $sr);

            if ($info["count"] == 1) {
                $result = array("displayname" => $info[0]['displayname'][0],
                    "samaccountname" => $info[0]['samaccountname'][0],
                    "mail" => $info[0]['mail'][0],
                    "rabo-id" => $info[0]['rabo-id'][0],
                    "thumbnailphoto" => base64_encode($info[0]['thumbnailphoto'][0]));
                session_start();
                $_SESSION['username'] = $result['samaccountname'];
                $_SESSION['userid'] = $result['rabo-id'];
                $_SESSION['userfullname'] = $result['displayname'];
                echo json_encode($result);
            }
        } else {
            echo "LDAP bind anonymous failed...";
        }
    } else {
		echo "No LDAP Connection...";
	}
}
?>